Si notas que el computador est� lento, que las aplicaciones no abren o se cierran solas, 
o que aparece la pantalla azul de Windows, pueden haber muchas causas, 
desde problemas de software (que se pueden prevenir), que el disco duro no est� funcionando correctamente, 
o que hay otra parte del hardware que est� dando se�ales de su mal funcionamiento.


Disco duro: si no est� funcionando bien o est� a punto de da�arse, los archivos que tienes guardados se volver�n corruptos, abrir algo tardar� demasiado tiempo o Windows podr�a dejar de funcionar completamente.

CPU: si est� demasiado caliente empezar� a fallar y aparecer� un pantallazo azul. Lo importante es saber cu�ndo aparece: �est�s jugando un juego que demande demasiado CPU? �editas un v�deo?

Tarjeta gr�fica: si falla mostrar� errores hasta en el escritorio de Windows, siempre de forma gr�fica. Tambi�n puede detener el driver de gr�ficos que uses, o dejar de funcionar completamente cuando juegas algo que est� en 3D.

Ventiladores: si estos presentan problemas, los componentes pueden recalentarse y el ordenador comenzar� a funcionar mal o incluso se apagar�a r�pidamente para poder enfriarse.

Tarjeta madre: muy dif�ciles de diagnosticar; es otro que necesita aplicar el m�todo de descartes para verificar que es esto lo que no funciona bien.

Fuente de poder: si ha dejado de funcionar el computador no prender� en lo absoluto.

RAM: otro componente dif�cil de diagnosticar porque presentar� pantallazos azules, archivos corruptos o aplicaciones que se cierran abruptamente.